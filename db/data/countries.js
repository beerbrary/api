module.exports = {
  'countries': {
    'country': [
      {
        'name': 'Andorra',
        'continent': 'EU',
        'iso': 'AND',
      },
      {
        'name': 'United Arab Emirates',
        'continent': 'AS',
        'iso': 'ARE',
      },
      {
        'name': 'Afghanistan',
        'continent': 'AS',
        'iso': 'AFG',
      },
      {
        'name': 'Antigua and Barbuda',
        'continent': 'NA',
        'iso': 'ATG',
      },
      {
        'name': 'Anguilla',
        'continent': 'NA',
        'iso': 'AIA',
      },
      {
        'name': 'Albania',
        'continent': 'EU',
        'iso': 'ALB',
      },
      {
        'name': 'Armenia',
        'continent': 'AS',
        'iso': 'ARM',
      },
      {
        'name': 'Angola',
        'continent': 'AF',
        'iso': 'AGO',
      },
      {
        'name': 'Antarctica',
        'continent': 'AN',
        'iso': 'ATA',
      },
      {
        'name': 'Argentina',
        'continent': 'SA',
        'iso': 'ARG',
      },
      {
        'name': 'American Samoa',
        'continent': 'OC',
        'iso': 'ASM',
      },
      {
        'name': 'Austria',
        'continent': 'EU',
        'iso': 'AUT',
      },
      {
        'name': 'Australia',
        'continent': 'OC',
        'iso': 'AUS',
      },
      {
        'name': 'Aruba',
        'continent': 'NA',
        'iso': 'ABW',
      },
      {
        'name': 'Åland',
        'continent': 'EU',
        'iso': 'ALA',
      },
      {
        'name': 'Azerbaijan',
        'continent': 'AS',
        'iso': 'AZE',
      },
      {
        'name': 'Bosnia and Herzegovina',
        'continent': 'EU',
        'iso': 'BIH',
      },
      {
        'name': 'Barbados',
        'continent': 'NA',
        'iso': 'BRB',
      },
      {
        'name': 'Bangladesh',
        'continent': 'AS',
        'iso': 'BGD',
      },
      {
        'name': 'Belgium',
        'continent': 'EU',
        'iso': 'BEL',
      },
      {
        'name': 'Burkina Faso',
        'continent': 'AF',
        'iso': 'BFA',
      },
      {
        'name': 'Bulgaria',
        'continent': 'EU',
        'iso': 'BGR',
      },
      {
        'name': 'Bahrain',
        'continent': 'AS',
        'iso': 'BHR',
      },
      {
        'name': 'Burundi',
        'continent': 'AF',
        'iso': 'BDI',
      },
      {
        'name': 'Benin',
        'continent': 'AF',
        'iso': 'BEN',
      },
      {
        'name': 'Saint Barthélemy',
        'continent': 'NA',
        'iso': 'BLM',
      },
      {
        'name': 'Bermuda',
        'continent': 'NA',
        'iso': 'BMU',
      },
      {
        'name': 'Brunei',
        'continent': 'AS',
        'iso': 'BRN',
      },
      {
        'name': 'Bolivia',
        'continent': 'SA',
        'iso': 'BOL',
      },
      {
        'name': 'Bonaire',
        'continent': 'NA',
        'iso': 'BES',
      },
      {
        'name': 'Brazil',
        'continent': 'SA',
        'iso': 'BRA',
      },
      {
        'name': 'Bahamas',
        'continent': 'NA',
        'iso': 'BHS',
      },
      {
        'name': 'Bhutan',
        'continent': 'AS',
        'iso': 'BTN',
      },
      {
        'name': 'Bouvet Island',
        'continent': 'AN',
        'iso': 'BVT',
      },
      {
        'name': 'Botswana',
        'continent': 'AF',
        'iso': 'BWA',
      },
      {
        'name': 'Belarus',
        'continent': 'EU',
        'iso': 'BLR',
      },
      {
        'name': 'Belize',
        'continent': 'NA',
        'iso': 'BLZ',
      },
      {
        'name': 'Canada',
        'continent': 'NA',
        'iso': 'CAN',
      },
      {
        'name': 'Cocos [Keeling] Islands',
        'continent': 'AS',
        'iso': 'CCK',
      },
      {
        'name': 'Democratic Republic of the Congo',
        'continent': 'AF',
        'iso': 'COD',
      },
      {
        'name': 'Central African Republic',
        'continent': 'AF',
        'iso': 'CAF',
      },
      {
        'name': 'Republic of the Congo',
        'continent': 'AF',
        'iso': 'COG',
      },
      {
        'name': 'Switzerland',
        'continent': 'EU',
        'iso': 'CHE',
      },
      {
        'name': 'Ivory Coast',
        'continent': 'AF',
        'iso': 'CIV',
      },
      {
        'name': 'Cook Islands',
        'continent': 'OC',
        'iso': 'COK',
      },
      {
        'name': 'Chile',
        'continent': 'SA',
        'iso': 'CHL',
      },
      {
        'name': 'Cameroon',
        'continent': 'AF',
        'iso': 'CMR',
      },
      {
        'name': 'China',
        'continent': 'AS',
        'iso': 'CHN',
      },
      {
        'name': 'Colombia',
        'continent': 'SA',
        'iso': 'COL',
      },
      {
        'name': 'Costa Rica',
        'continent': 'NA',
        'iso': 'CRI',
      },
      {
        'name': 'Cuba',
        'continent': 'NA',
        'iso': 'CUB',
      },
      {
        'name': 'Cape Verde',
        'continent': 'AF',
        'iso': 'CPV',
      },
      {
        'name': 'Curacao',
        'continent': 'NA',
        'iso': 'CUW',
      },
      {
        'name': 'Christmas Island',
        'continent': 'OC',
        'iso': 'CXR',
      },
      {
        'name': 'Cyprus',
        'continent': 'EU',
        'iso': 'CYP',
      },
      {
        'name': 'Czechia',
        'continent': 'EU',
        'iso': 'CZE',
      },
      {
        'name': 'Germany',
        'continent': 'EU',
        'iso': 'DEU',
      },
      {
        'name': 'Djibouti',
        'continent': 'AF',
        'iso': 'DJI',
      },
      {
        'name': 'Denmark',
        'continent': 'EU',
        'iso': 'DNK',
      },
      {
        'name': 'Dominica',
        'continent': 'NA',
        'iso': 'DMA',
      },
      {
        'name': 'Dominican Republic',
        'continent': 'NA',
        'iso': 'DOM',
      },
      {
        'name': 'Algeria',
        'continent': 'AF',
        'iso': 'DZA',
      },
      {
        'name': 'Ecuador',
        'continent': 'SA',
        'iso': 'ECU',
      },
      {
        'name': 'Estonia',
        'continent': 'EU',
        'iso': 'EST',
      },
      {
        'name': 'Egypt',
        'continent': 'AF',
        'iso': 'EGY',
      },
      {
        'name': 'Western Sahara',
        'continent': 'AF',
        'iso': 'ESH',
      },
      {
        'name': 'Eritrea',
        'continent': 'AF',
        'iso': 'ERI',
      },
      {
        'name': 'Spain',
        'continent': 'EU',
        'iso': 'ESP',
      },
      {
        'name': 'Ethiopia',
        'continent': 'AF',
        'iso': 'ETH',
      },
      {
        'name': 'Finland',
        'continent': 'EU',
        'iso': 'FIN',
      },
      {
        'name': 'Fiji',
        'continent': 'OC',
        'iso': 'FJI',
      },
      {
        'name': 'Falkland Islands',
        'continent': 'SA',
        'iso': 'FLK',
      },
      {
        'name': 'Micronesia',
        'continent': 'OC',
        'iso': 'FSM',
      },
      {
        'name': 'Faroe Islands',
        'continent': 'EU',
        'iso': 'FRO',
      },
      {
        'name': 'France',
        'continent': 'EU',
        'iso': 'FRA',
      },
      {
        'name': 'Gabon',
        'continent': 'AF',
        'iso': 'GAB',
      },
      {
        'name': 'United Kingdom',
        'continent': 'EU',
        'iso': 'GBR',
      },
      {
        'name': 'Grenada',
        'continent': 'NA',
        'iso': 'GRD',
      },
      {
        'name': 'Georgia',
        'continent': 'AS',
        'iso': 'GEO',
      },
      {
        'name': 'French Guiana',
        'continent': 'SA',
        'iso': 'GUF',
      },
      {
        'name': 'Guernsey',
        'continent': 'EU',
        'iso': 'GGY',
      },
      {
        'name': 'Ghana',
        'continent': 'AF',
        'iso': 'GHA',
      },
      {
        'name': 'Gibraltar',
        'continent': 'EU',
        'iso': 'GIB',
      },
      {
        'name': 'Greenland',
        'continent': 'NA',
        'iso': 'GRL',
      },
      {
        'name': 'Gambia',
        'continent': 'AF',
        'iso': 'GMB',
      },
      {
        'name': 'Guinea',
        'continent': 'AF',
        'iso': 'GIN',
      },
      {
        'name': 'Guadeloupe',
        'continent': 'NA',
        'iso': 'GLP',
      },
      {
        'name': 'Equatorial Guinea',
        'continent': 'AF',
        'iso': 'GNQ',
      },
      {
        'name': 'Greece',
        'continent': 'EU',
        'iso': 'GRC',
      },
      {
        'name': 'South Georgia and the South Sandwich Islands',
        'continent': 'AN',
        'iso': 'SGS',
      },
      {
        'name': 'Guatemala',
        'continent': 'NA',
        'iso': 'GTM',
      },
      {
        'name': 'Guam',
        'continent': 'OC',
        'iso': 'GUM',
      },
      {
        'name': 'Guinea-Bissau',
        'continent': 'AF',
        'iso': 'GNB',
      },
      {
        'name': 'Guyana',
        'continent': 'SA',
        'iso': 'GUY',
      },
      {
        'name': 'Hong Kong',
        'continent': 'AS',
        'iso': 'HKG',
      },
      {
        'name': 'Heard Island and McDonald Islands',
        'continent': 'AN',
        'iso': 'HMD',
      },
      {
        'name': 'Honduras',
        'continent': 'NA',
        'iso': 'HND',
      },
      {
        'name': 'Croatia',
        'continent': 'EU',
        'iso': 'HRV',
      },
      {
        'name': 'Haiti',
        'continent': 'NA',
        'iso': 'HTI',
      },
      {
        'name': 'Hungary',
        'continent': 'EU',
        'iso': 'HUN',
      },
      {
        'name': 'Indonesia',
        'continent': 'AS',
        'iso': 'IDN',
      },
      {
        'name': 'Ireland',
        'continent': 'EU',
        'iso': 'IRL',
      },
      {
        'name': 'Israel',
        'continent': 'AS',
        'iso': 'ISR',
      },
      {
        'name': 'Isle of Man',
        'continent': 'EU',
        'iso': 'IMN',
      },
      {
        'name': 'India',
        'continent': 'AS',
        'iso': 'IND',
      },
      {
        'name': 'British Indian Ocean Territory',
        'continent': 'AS',
        'iso': 'IOT',
      },
      {
        'name': 'Iraq',
        'continent': 'AS',
        'iso': 'IRQ',
      },
      {
        'name': 'Iran',
        'continent': 'AS',
        'iso': 'IRN',
      },
      {
        'name': 'Iceland',
        'continent': 'EU',
        'iso': 'ISL',
      },
      {
        'name': 'Italy',
        'continent': 'EU',
        'iso': 'ITA',
      },
      {
        'name': 'Jersey',
        'continent': 'EU',
        'iso': 'JEY',
      },
      {
        'name': 'Jamaica',
        'continent': 'NA',
        'iso': 'JAM',
      },
      {
        'name': 'Jordan',
        'continent': 'AS',
        'iso': 'JOR',
      },
      {
        'name': 'Japan',
        'continent': 'AS',
        'iso': 'JPN',
      },
      {
        'name': 'Kenya',
        'continent': 'AF',
        'iso': 'KEN',
      },
      {
        'name': 'Kyrgyzstan',
        'continent': 'AS',
        'iso': 'KGZ',
      },
      {
        'name': 'Cambodia',
        'continent': 'AS',
        'iso': 'KHM',
      },
      {
        'name': 'Kiribati',
        'continent': 'OC',
        'iso': 'KIR',
      },
      {
        'name': 'Comoros',
        'continent': 'AF',
        'iso': 'COM',
      },
      {
        'name': 'Saint Kitts and Nevis',
        'continent': 'NA',
        'iso': 'KNA',
      },
      {
        'name': 'North Korea',
        'continent': 'AS',
        'iso': 'PRK',
      },
      {
        'name': 'South Korea',
        'continent': 'AS',
        'iso': 'KOR',
      },
      {
        'name': 'Kuwait',
        'continent': 'AS',
        'iso': 'KWT',
      },
      {
        'name': 'Cayman Islands',
        'continent': 'NA',
        'iso': 'CYM',
      },
      {
        'name': 'Kazakhstan',
        'continent': 'AS',
        'iso': 'KAZ',
      },
      {
        'name': 'Laos',
        'continent': 'AS',
        'iso': 'LAO',
      },
      {
        'name': 'Lebanon',
        'continent': 'AS',
        'iso': 'LBN',
      },
      {
        'name': 'Saint Lucia',
        'continent': 'NA',
        'iso': 'LCA',
      },
      {
        'name': 'Liechtenstein',
        'continent': 'EU',
        'iso': 'LIE',
      },
      {
        'name': 'Sri Lanka',
        'continent': 'AS',
        'iso': 'LKA',
      },
      {
        'name': 'Liberia',
        'continent': 'AF',
        'iso': 'LBR',
      },
      {
        'name': 'Lesotho',
        'continent': 'AF',
        'iso': 'LSO',
      },
      {
        'name': 'Lithuania',
        'continent': 'EU',
        'iso': 'LTU',
      },
      {
        'name': 'Luxembourg',
        'continent': 'EU',
        'iso': 'LUX',
      },
      {
        'name': 'Latvia',
        'continent': 'EU',
        'iso': 'LVA',
      },
      {
        'name': 'Libya',
        'continent': 'AF',
        'iso': 'LBY',
      },
      {
        'name': 'Morocco',
        'continent': 'AF',
        'iso': 'MAR',
      },
      {
        'name': 'Monaco',
        'continent': 'EU',
        'iso': 'MCO',
      },
      {
        'name': 'Moldova',
        'continent': 'EU',
        'iso': 'MDA',
      },
      {
        'name': 'Montenegro',
        'continent': 'EU',
        'iso': 'MNE',
      },
      {
        'name': 'Saint Martin',
        'continent': 'NA',
        'iso': 'MAF',
      },
      {
        'name': 'Madagascar',
        'continent': 'AF',
        'iso': 'MDG',
      },
      {
        'name': 'Marshall Islands',
        'continent': 'OC',
        'iso': 'MHL',
      },
      {
        'name': 'Macedonia',
        'continent': 'EU',
        'iso': 'MKD',
      },
      {
        'name': 'Mali',
        'continent': 'AF',
        'iso': 'MLI',
      },
      {
        'name': 'Myanmar [Burma]',
        'continent': 'AS',
        'iso': 'MMR',
      },
      {
        'name': 'Mongolia',
        'continent': 'AS',
        'iso': 'MNG',
      },
      {
        'name': 'Macao',
        'continent': 'AS',
        'iso': 'MAC',
      },
      {
        'name': 'Northern Mariana Islands',
        'continent': 'OC',
        'iso': 'MNP',
      },
      {
        'name': 'Martinique',
        'continent': 'NA',
        'iso': 'MTQ',
      },
      {
        'name': 'Mauritania',
        'continent': 'AF',
        'iso': 'MRT',
      },
      {
        'name': 'Montserrat',
        'continent': 'NA',
        'iso': 'MSR',
      },
      {
        'name': 'Malta',
        'continent': 'EU',
        'iso': 'MLT',
      },
      {
        'name': 'Mauritius',
        'continent': 'AF',
        'iso': 'MUS',
      },
      {
        'name': 'Maldives',
        'continent': 'AS',
        'iso': 'MDV',
      },
      {
        'name': 'Malawi',
        'continent': 'AF',
        'iso': 'MWI',
      },
      {
        'name': 'Mexico',
        'continent': 'NA',
        'iso': 'MEX',
      },
      {
        'name': 'Malaysia',
        'continent': 'AS',
        'iso': 'MYS',
      },
      {
        'name': 'Mozambique',
        'continent': 'AF',
        'iso': 'MOZ',
      },
      {
        'name': 'Namibia',
        'continent': 'AF',
        'iso': 'NAM',
      },
      {
        'name': 'New Caledonia',
        'continent': 'OC',
        'iso': 'NCL',
      },
      {
        'name': 'Niger',
        'continent': 'AF',
        'iso': 'NER',
      },
      {
        'name': 'Norfolk Island',
        'continent': 'OC',
        'iso': 'NFK',
      },
      {
        'name': 'Nigeria',
        'continent': 'AF',
        'iso': 'NGA',
      },
      {
        'name': 'Nicaragua',
        'continent': 'NA',
        'iso': 'NIC',
      },
      {
        'name': 'Netherlands',
        'continent': 'EU',
        'iso': 'NLD',
      },
      {
        'name': 'Norway',
        'continent': 'EU',
        'iso': 'NOR',
      },
      {
        'name': 'Nepal',
        'continent': 'AS',
        'iso': 'NPL',
      },
      {
        'name': 'Nauru',
        'continent': 'OC',
        'iso': 'NRU',
      },
      {
        'name': 'Niue',
        'continent': 'OC',
        'iso': 'NIU',
      },
      {
        'name': 'New Zealand',
        'continent': 'OC',
        'iso': 'NZL',
      },
      {
        'name': 'Oman',
        'continent': 'AS',
        'iso': 'OMN',
      },
      {
        'name': 'Panama',
        'continent': 'NA',
        'iso': 'PAN',
      },
      {
        'name': 'Peru',
        'continent': 'SA',
        'iso': 'PER',
      },
      {
        'name': 'French Polynesia',
        'continent': 'OC',
        'iso': 'PYF',
      },
      {
        'name': 'Papua New Guinea',
        'continent': 'OC',
        'iso': 'PNG',
      },
      {
        'name': 'Philippines',
        'continent': 'AS',
        'iso': 'PHL',
      },
      {
        'name': 'Pakistan',
        'continent': 'AS',
        'iso': 'PAK',
      },
      {
        'name': 'Poland',
        'continent': 'EU',
        'iso': 'POL',
      },
      {
        'name': 'Saint Pierre and Miquelon',
        'continent': 'NA',
        'iso': 'SPM',
      },
      {
        'name': 'Pitcairn Islands',
        'continent': 'OC',
        'iso': 'PCN',
      },
      {
        'name': 'Puerto Rico',
        'continent': 'NA',
        'iso': 'PRI',
      },
      {
        'name': 'Palestine',
        'continent': 'AS',
        'iso': 'PSE',
      },
      {
        'name': 'Portugal',
        'continent': 'EU',
        'iso': 'PRT',
      },
      {
        'name': 'Palau',
        'continent': 'OC',
        'iso': 'PLW',
      },
      {
        'name': 'Paraguay',
        'continent': 'SA',
        'iso': 'PRY',
      },
      {
        'name': 'Qatar',
        'continent': 'AS',
        'iso': 'QAT',
      },
      {
        'name': 'Réunion',
        'continent': 'AF',
        'iso': 'REU',
      },
      {
        'name': 'Romania',
        'continent': 'EU',
        'iso': 'ROU',
      },
      {
        'name': 'Serbia',
        'continent': 'EU',
        'iso': 'SRB',
      },
      {
        'name': 'Russia',
        'continent': 'EU',
        'iso': 'RUS',
      },
      {
        'name': 'Rwanda',
        'continent': 'AF',
        'iso': 'RWA',
      },
      {
        'name': 'Saudi Arabia',
        'continent': 'AS',
        'iso': 'SAU',
      },
      {
        'name': 'Solomon Islands',
        'continent': 'OC',
        'iso': 'SLB',
      },
      {
        'name': 'Seychelles',
        'continent': 'AF',
        'iso': 'SYC',
      },
      {
        'name': 'Sudan',
        'continent': 'AF',
        'iso': 'SDN',
      },
      {
        'name': 'Sweden',
        'continent': 'EU',
        'iso': 'SWE',
      },
      {
        'name': 'Singapore',
        'continent': 'AS',
        'iso': 'SGP',
      },
      {
        'name': 'Saint Helena',
        'continent': 'AF',
        'iso': 'SHN',
      },
      {
        'name': 'Slovenia',
        'continent': 'EU',
        'iso': 'SVN',
      },
      {
        'name': 'Svalbard and Jan Mayen',
        'continent': 'EU',
        'iso': 'SJM',
      },
      {
        'name': 'Slovakia',
        'continent': 'EU',
        'iso': 'SVK',
      },
      {
        'name': 'Sierra Leone',
        'continent': 'AF',
        'iso': 'SLE',
      },
      {
        'name': 'San Marino',
        'continent': 'EU',
        'iso': 'SMR',
      },
      {
        'name': 'Senegal',
        'continent': 'AF',
        'iso': 'SEN',
      },
      {
        'name': 'Somalia',
        'continent': 'AF',
        'iso': 'SOM',
      },
      {
        'name': 'Suriname',
        'continent': 'SA',
        'iso': 'SUR',
      },
      {
        'name': 'South Sudan',
        'continent': 'AF',
        'iso': 'SSD',
      },
      {
        'name': 'São Tomé and Príncipe',
        'continent': 'AF',
        'iso': 'STP',
      },
      {
        'name': 'El Salvador',
        'continent': 'NA',
        'iso': 'SLV',
      },
      {
        'name': 'Sint Maarten',
        'continent': 'NA',
        'iso': 'SXM',
      },
      {
        'name': 'Syria',
        'continent': 'AS',
        'iso': 'SYR',
      },
      {
        'name': 'Swaziland',
        'continent': 'AF',
        'iso': 'SWZ',
      },
      {
        'name': 'Turks and Caicos Islands',
        'continent': 'NA',
        'iso': 'TCA',
      },
      {
        'name': 'Chad',
        'continent': 'AF',
        'iso': 'TCD',
      },
      {
        'name': 'French Southern Territories',
        'continent': 'AN',
        'iso': 'ATF',
      },
      {
        'name': 'Togo',
        'continent': 'AF',
        'iso': 'TGO',
      },
      {
        'name': 'Thailand',
        'continent': 'AS',
        'iso': 'THA',
      },
      {
        'name': 'Tajikistan',
        'continent': 'AS',
        'iso': 'TJK',
      },
      {
        'name': 'Tokelau',
        'continent': 'OC',
        'iso': 'TKL',
      },
      {
        'name': 'East Timor',
        'continent': 'OC',
        'iso': 'TLS',
      },
      {
        'name': 'Turkmenistan',
        'continent': 'AS',
        'iso': 'TKM',
      },
      {
        'name': 'Tunisia',
        'continent': 'AF',
        'iso': 'TUN',
      },
      {
        'name': 'Tonga',
        'continent': 'OC',
        'iso': 'TON',
      },
      {
        'name': 'Turkey',
        'continent': 'AS',
        'iso': 'TUR',
      },
      {
        'name': 'Trinidad and Tobago',
        'continent': 'NA',
        'iso': 'TTO',
      },
      {
        'name': 'Tuvalu',
        'continent': 'OC',
        'iso': 'TUV',
      },
      {
        'name': 'Taiwan',
        'continent': 'AS',
        'iso': 'TWN',
      },
      {
        'name': 'Tanzania',
        'continent': 'AF',
        'iso': 'TZA',
      },
      {
        'name': 'Ukraine',
        'continent': 'EU',
        'iso': 'UKR',
      },
      {
        'name': 'Uganda',
        'continent': 'AF',
        'iso': 'UGA',
      },
      {
        'name': 'U.S. Minor Outlying Islands',
        'continent': 'OC',
        'iso': 'UMI',
      },
      {
        'name': 'United States',
        'continent': 'NA',
        'iso': 'USA',
      },
      {
        'name': 'Uruguay',
        'continent': 'SA',
        'iso': 'URY',
      },
      {
        'name': 'Uzbekistan',
        'continent': 'AS',
        'iso': 'UZB',
      },
      {
        'name': 'Vatican City',
        'continent': 'EU',
        'iso': 'VAT',
      },
      {
        'name': 'Saint Vincent and the Grenadines',
        'continent': 'NA',
        'iso': 'VCT',
      },
      {
        'name': 'Venezuela',
        'continent': 'SA',
        'iso': 'VEN',
      },
      {
        'name': 'British Virgin Islands',
        'continent': 'NA',
        'iso': 'VGB',
      },
      {
        'name': 'U.S. Virgin Islands',
        'continent': 'NA',
        'iso': 'VIR',
      },
      {
        'name': 'Vietnam',
        'continent': 'AS',
        'iso': 'VNM',
      },
      {
        'name': 'Vanuatu',
        'continent': 'OC',
        'iso': 'VUT',
      },
      {
        'name': 'Wallis and Futuna',
        'continent': 'OC',
        'iso': 'WLF',
      },
      {
        'name': 'Samoa',
        'continent': 'OC',
        'iso': 'WSM',
      },
      {
        'name': 'Kosovo',
        'continent': 'EU',
        'iso': 'XKX',
      },
      {
        'name': 'Yemen',
        'continent': 'AS',
        'iso': 'YEM',
      },
      {
        'name': 'Mayotte',
        'continent': 'AF',
        'iso': 'MYT',
      },
      {
        'name': 'South Africa',
        'continent': 'AF',
        'iso': 'ZAF',
      },
      {
        'name': 'Zambia',
        'continent': 'AF',
        'iso': 'ZMB',
      },
      {
        'name': 'Zimbabwe',
        'continent': 'AF',
        'iso': 'ZWE',
      },
    ],
  },
}
