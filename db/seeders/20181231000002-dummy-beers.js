module.exports = {
  up: function (queryInterface) {
    let estrellaDamm = {
      name: 'Estrella Damm',
      normalized_name: 'estrella-damm',
      alcohol_content: 5.4,
      type: 'lager',
      brewer_id: 1,
      created_at: new Date(),
      updated_at: new Date(),
    }

    let bockDamm = {
      name: 'Bock-Damm',
      normalized_name: 'bockdamm',
      alcohol_content: 5.9,
      type: 'lager',
      brewer_id: 1,
      created_at: new Date(),
      updated_at: new Date(),
    }

    let xibeca = {
      name: 'Xibeca',
      normalized_name: 'xibeca',
      alcohol_content: 4.6,
      type: 'lager',
      brewer_id: 1,
      created_at: new Date(),
      updated_at: new Date(),
    }

    let estrellaDammDaura = {
      name: 'Daura Damm',
      normalized_name: 'daura-damm',
      alcohol_content: 5.4,
      type: 'lager',
      brewer_id: 1,
      created_at: new Date(),
      updated_at: new Date(),
    }

    return queryInterface.bulkInsert('beer', [
      estrellaDamm,
      bockDamm,
      xibeca,
      estrellaDammDaura,
    ])
  },

  down: function (queryInterface) {
    return queryInterface.bulkDelete('beer', null, {})
  },
}
