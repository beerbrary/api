module.exports = {

  up(queryInterface, Sequelize) {
    return queryInterface.addColumn('brewer', 'country_id', {
      type: Sequelize.INTEGER,
      onDelete: 'cascade',
      allowNull: false,
      references: {
        model: 'country',
        key: 'id',
      },
    })
  },

  down(queryInterface) {
    return queryInterface.removeColumn('brewer', 'country_id')
  },

}
