import Sequelize from 'sequelize'

export default class Country extends Sequelize.Model {
  static init(sequelize) {
    return super.init({
      name: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      normalized_name: {
        allowNull: false,
        unique: true,
        type: Sequelize.STRING,
      },
      iso: {
        allowNull: false,
        unique: true,
        type: Sequelize.STRING,
      },
      continent: {
        type: Sequelize.ENUM,
        values: [
          'AF', // Africa
          'AS', // Asia
          'EU', // Europe
          'NA', // North America
          'OC', // Oceania
          'SA', // South America
          'AN', // Antarctica
        ],
      },
    }, {
      underscored: true,
      timestamps: false,
      tableName: 'country',
      sequelize,
    })
  }

  static associate(models) {
    this.brewers = this.hasMany(models.Brewer)
  }
}
