var slugify = require('slugify')
var seeds = require('./../data/countries.js')

module.exports = {
  up: function (queryInterface) {
    for (var i in seeds.countries.country) {
      var country = seeds.countries.country[i]
      country.normalized_name = slugify(country.name, {
        lower: true,
      })
    }

    return queryInterface.bulkInsert('country', seeds.countries.country)
  },

  down: function (queryInterface) {
    return queryInterface.bulkDelete('Country', null, {})
  },
}
