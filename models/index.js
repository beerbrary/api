import { readdirSync } from 'fs'
import { basename, join } from 'path'

import Sequelize from 'sequelize'

var baseFilename = basename(module.filename)
var env = process.env.NODE_ENV || 'development'
var config = require(__dirname + '/../config/database.js')[env]
var sequelize = new Sequelize(config.database, config.username, config.password, config)

const models = {}

readdirSync(__dirname)
  .filter(file => file.indexOf('.') !== 0 && file !== baseFilename)
  .forEach(file => {
    const model = require(join(__dirname, file)).default
    models[model.name] = model.init(sequelize)
  })

Object.keys(models)
  .filter(modelName => typeof models[modelName].associate === 'function')
  .forEach(modelName => models[modelName].associate(models))

models.Op = Sequelize.Op

export default models
