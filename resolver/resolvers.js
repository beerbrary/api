import { resolver } from 'graphql-sequelize'

import models from './../models'

const Op = models.Op

const resolvers = {
  Query: {
    countries: resolver(models.Country),
    beers: resolver(models.Beer, {
      before: (findOptions, args) => {
        findOptions.order = [['name', 'ASC']]
        if (args.query) {
          findOptions.where = {
            name: { [Op.iLike]: `%${args.query}%` },
          }
        }

        return findOptions
      },
    }),
  },
  Country: {
    brewers: resolver(models.Country.brewers),
  },
  Brewer: {
    country: resolver(models.Brewer.country),
    beers: resolver(models.Brewer.beers),
  },
  Beer: resolver(models.Beer.brewer),
}

export default resolvers
