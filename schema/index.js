import Root from './root.graphql'
import Query from './query.graphql'
import Type from './type.graphql'

export default [Root, Query, Type]
