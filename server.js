import express from 'express'
import { ApolloServer } from 'apollo-server'
import { registerServer } from 'apollo-server-express'

import models from './models'
import typeDefs from './schema'
import resolvers from './resolver'

async function checkHealthstatus() {
  let services = {}

  try {
    await models.sequelize.authenticate()
    services.postgresql = 'up'
  } catch (e) {
    services.postgresql = 'down'
  }

  return services
}

const app = express()

app.get('/', (req, res) => res.send('The finest beer API!'))

app.get('/status', (req, res) => {
  checkHealthstatus().then(services => res.json(services))
})

const server = new ApolloServer({
  typeDefs,
  resolvers,
})
registerServer({ server, app })

server.listen({ http: { port: 4000 }})
  .then(() => {
    // pass
  })
