import Sequelize from 'sequelize'

export default class Beer extends Sequelize.Model {
  static init(sequelize) {
    return super.init({
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      normalized_name: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      alcohol_content: Sequelize.DECIMAL(3, 2),
      type: Sequelize.ENUM('ale', 'lager', 'pilsner'),
    }, {
      underscored: true,
      tableName: 'beer',
      sequelize,
    })
  }

  static associate(models) {
    this.brewer = this.belongsTo(models.Brewer)
  }
}
