const Op = require('sequelize').Op

const defaultConfig = {
  'username': process.env.POSTGRES_USER,
  'password': process.env.POSTGRES_PASSWORD,
  'database': process.env.POSTGRES_DB,
  'host': process.env.POSTGRES_HOST,
  'dialect': 'postgres',
  'operatorsAliases': Op,
}

module.exports = {
  'development': defaultConfig,
  'production': defaultConfig,
}
