FROM node:8.2-alpine

MAINTAINER Lluc Martorell <superpesi@gmail.com>

WORKDIR /app

ADD package.json /app

RUN npm install