import Sequelize from 'sequelize'

export default class Brewer extends Sequelize.Model {
  static init(sequelize) {
    return super.init({
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      normalized_name: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      logo_path: Sequelize.TEXT,
    }, {
      underscored: true,
      timestamps: false,
      tableName: 'brewer',
      sequelize,
    })
  }

  static associate(models) {
    this.beers = this.hasMany(models.Beer)
    this.country = this.belongsTo(models.Country)
  }
}
