module.exports = {

  up(queryInterface, Sequelize) {
    return queryInterface.createTable('beer', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      normalized_name: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      alcohol_content: {
        type: Sequelize.DECIMAL(3, 2),
      },
      type: {
        type: Sequelize.ENUM('ale', 'lager', 'pilsner'),
      },
      brewer_id: {
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        allowNull: false,
        references: {
          model: 'brewer',
          key: 'id',
        },
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    })
  },

  down(queryInterface) {
    return queryInterface.dropTable('beer')
  },

}
