module.exports = {

  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('brewer', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      normalized_name: {
        allowNull: false,
        unique: true,
        type: Sequelize.STRING,
      },
      logo_path: {
        type: Sequelize.TEXT,
      },
    })
  },

  down: function(queryInterface) {
    return queryInterface.dropTable('brewer')
  },

}
