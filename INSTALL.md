# Beerbrary API

Npm install
```
$ docker-compose run --rm npm i
```

Up db
```
$ docker-compose up -d db
```

Migrate
```
$ docker-compose run --rm sequelize db:migrate
```

Seed
```
$ docker-compose run --rm sequelize db:seed:all
```

Up api
```
$ docker-compose up -d api
```
