module.exports = {
  up: function (queryInterface) {
    let damm = {
      name: 'S.A. Damm',
      normalized_name: 'sa-damm',
      country_id: 68, // Spain
      logo_path: '',
    }

    let rivera = {
      name: 'Hijos de Rivera S.A.',
      normalized_name: 'hijos-de-rivera-sa',
      country_id: 68, // Spain
      logo_path: '',
    }

    let sanMiguel = {
      name: 'San Miguel, Fábricas de Cerveza, S.L.U',
      normalized_name: 'san-miguel-fabricas-de-cerveza-slu',
      country_id: 68, // Spain
      logo_path: '',
    }

    let alhambra = {
      name: 'Cervezas Alhambra S.L.',
      normalized_name: 'cervezas-alhambra-sl',
      country_id: 68, // Spain
      logo_path: '',
    }

    let franziskaner = {
      name: 'Spaten-Franziskaner-Bräu GmbH',
      normalized_name: 'spaten-franziskaner-brau-gmbh',
      country_id: 57, // Germany
      logo_path: '',
    }

    let hoegaarden = {
      name: 'Hoegaarden',
      normalized_name: 'hoegaarden',
      country_id: 20, // Belgium
      logo_path: '',
    }

    return queryInterface.bulkInsert('brewer', [
      damm,
      rivera,
      sanMiguel,
      alhambra,
      franziskaner,
      hoegaarden,
    ])
  },

  down: function (queryInterface) {
    return queryInterface.bulkDelete('brewer', null, {})
  },
}
