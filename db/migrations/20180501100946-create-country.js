module.exports = {

  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('country', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      normalized_name: {
        allowNull: false,
        unique: true,
        type: Sequelize.STRING,
      },
      iso: {
        allowNull: false,
        unique: true,
        type: Sequelize.STRING,
      },
      continent: {
        type: Sequelize.ENUM,
        values: ['AF', 'AS', 'EU', 'NA', 'OC', 'SA', 'AN'],
      },
    })
  },

  down: function(queryInterface) {
    return queryInterface.dropTable('country')
  },

}
